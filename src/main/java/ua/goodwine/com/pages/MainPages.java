package ua.goodwine.com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import ua.goodwine.com.tests.MainTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertTrue;

public class MainPages {

    public MainPages(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public MainTest mainTest;
    public WebDriver driver;

    protected boolean isPageload(int loadTime,String errorMassage){
        return isElementPresent(By.cssSelector("body"),loadTime,errorMassage); // TODO change to method wait for page load
    }


    protected boolean isElementPresent(By locator, int delaySeconds, String errorMassage){
        driver.manage().timeouts().implicitlyWait(delaySeconds, TimeUnit.SECONDS);
        boolean result = driver.findElements(locator).size() > 0;
        driver.manage().timeouts().implicitlyWait(mainTest.DefaultDelay,TimeUnit.SECONDS);
        assertTrue(errorMassage,result);
        return result;
    }

    protected WebElement getElement(By locator, int delaySeconds, String errorMassage){
        boolean result = isElementPresent(locator,delaySeconds,errorMassage);
        if (result) {
            return driver.findElements(locator).get(0);
        } else {
            return null; // TODO change on some
        }
    }
    protected boolean waitForUrlToContainString (String URL, int delaySeconds, String errorMassage){
//        driver.manage().timeouts().implicitlyWait(delaySeconds, TimeUnit.SECONDS);
        String currentUrl = driver.getCurrentUrl();
        boolean isPageLoad = isPageload(delaySeconds,errorMassage);

        if(currentUrl.contains(URL) && isPageLoad){

            assertTrue(errorMassage,true);
            return true;
        } else {

            assertTrue(errorMassage,false);
            return false;
        }

    }


}
