package ua.goodwine.com.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainMenu extends MainPages {
    public MainMenu(WebDriver driver) {
        super(driver);
    }

    @Step("Click on first menu 'Vine'")
    public void ClickOnFirstMenu(String massage){
        getElement(By.xpath("//a[@class='level-top dropdown'][contains(text(),'Вино')]"),5,"Vine element in menu not clickable").click();
        waitForUrlToContainString("/vino",15,"URL not contained '/vino'");
        System.out.println(driver.getCurrentUrl());
    }
}
